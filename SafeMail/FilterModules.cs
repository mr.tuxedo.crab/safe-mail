﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using OpenPop.Mime;
using System.Windows.Media;
using System.IO;
using Spire.Pdf;
using Tesseract;

namespace SafeMail
{
    public class Responce
    {
        public bool Passed { get; set; }
        public string Result { get; set; }
        public Brush ResColor { get; set; }
        public string Feedback { get; set; }
        public string ModuleName { get; set; }
        public string FileType { get; set; }

        public Responce()
        {
            Passed = true;
            Feedback = "";
            ModuleName = "";
            FileType = null;
            UpdateFields();
        }

        public Responce(string moduleName)
        {
            Passed = true;
            Feedback = "";
            ModuleName = moduleName;
            FileType = null;
            UpdateFields();
        }
        public void UpdateFields()
        {
            Result = Passed ? "Прошел" : "Не прошел";
            ResColor = Passed ? Brushes.Green : Brushes.Red;
            if (Feedback.EndsWith("\n")) Feedback = Feedback.Substring(0, Feedback.LastIndexOf('\n'));
        }
    }
    public class FilterModules
    {
        public static Responce Checker_Location(MainWindow caller, string ip) //возвращает true, если тест пройден (либо страна есть в whitelist, либо ее нет в blacklist)
        {
            bool isBlacklist = caller.locationComboBox.SelectedIndex == 0;
            List<string> countries = caller.locationTextbox.Text.Split(',').ToList();

            Responce res = new Responce("Модуль геолокации");

            string strFile;
            using (var objClient = new WebClient())
            {
                strFile = objClient.DownloadString("http://www.geoplugin.net/json.gp?ip=" + ip);
            }
            dynamic responce = JsonConvert.DeserializeObject(strFile);
            try
            {
                string cc = (string)responce.geoplugin_countryCode;
                res.Passed = countries.Contains(cc) != isBlacklist;
                if (res.Passed)
                    res.Feedback = string.Format("Страна хоста отправителя ({0}) {1}",
                        (string)responce.geoplugin_countryName,
                        isBlacklist ? "отсутствует в черном списке" : "присутствует в белом списке");
                else
                    res.Feedback = string.Format("Страна хоста отправителя ({0}) {1}",
                        (string)responce.geoplugin_countryName,
                        isBlacklist ? "присутсвует в черном списке" : "отсутствует в белом списке");
            }
            catch (Exception e)
            {
                res.Passed = false;
                res.Feedback = string.Format("API проверки вернуло ошибку; В целях безопасности считается, что страна отправителя {0}" +
                    "\nПолный код ошибки представлен ниже\n\n{1}",
                    isBlacklist ? "присутсвует в черном списке" : "отсутствует в белом списке",
                    e.ToString());
            }
            res.UpdateFields();
            return res;
        }

        public static Responce Checker_DNSBL(string ip)
        {
            var services = new List<string>() { "zen.spamhaus.org"/*, "bl.spamcop.net"*/ };
            string rev_ip = string.Join(".", ip.Split('.').Reverse());

            Responce res = new Responce("Модуль DNSBL");
            foreach (string serv in services)
            {
                try
                {
                    IPHostEntry ipEntry = Dns.GetHostEntry(rev_ip + "." + serv);
                    ipEntry = null;
                    res.Passed = false; //если удалось получить IPHostEntry, то запись есть в базе
                    res.Feedback += string.Format("Запись найдена в DNSBL {0}\n", serv);
                }
                catch (System.Net.Sockets.SocketException dnserr)
                {
                    if (dnserr.ErrorCode == 11001) res.Feedback += string.Format("IP отправителя не был найден в DNSBL {0}\n", serv);
                    else res.Feedback += string.Format("DNSBL {0} вернул код ошибки {1}: {2}\n", serv, dnserr.ErrorCode.ToString(), dnserr.Message);
                }
            }
            res.UpdateFields();
            return res;
        }

        public static Responce Checker_File(MessagePart file)
        {
            Responce res = new Responce("Модуль проверки файла");

            string raw_type = file.ContentType.ToString().Split(';')[0];
            res.FileType = ClassifyFile(raw_type);

            switch (res.FileType)
            {
                case "audio":
                    res.Feedback = "Файл классифицирован как аудио-файл и не представляет опасности";
                    break;
                case "video":
                    res.Feedback = "Файл классифицирован как видео-файл и не представляет опасности";
                    break;
                case "font":
                    res.Feedback = "Файл классифицирован как файл шрифта и не представляет опасности";
                    break;
                case "image":
                    res.Feedback = "Файл классифицирован как изображение. Сам файл не представляет опасности, но может содержать опасный текст и/или ссылки";
                    break;
                case "text":
                    res.Feedback = "Файл классифицирован как текстовый. Сам файл не представляет опасности, но может содержать опасный текст и/или ссылки, либо содержать исполняемый код, представляющий опасность для устройства";
                    break;
                case "application":
                    res.Passed = false;
                    res.Feedback = "Файл классифицирован как приложение или исполняемый файл. Файл представляет потенциальную опасность";
                    break;
                case "archive":
                    res.Passed = false;
                    res.Feedback = "Файл классифицирован как архив. Файл представляет потенциальную опасность, так как может содержать другие " +
                        "потенциально опасные файлы";
                    break;
                case "word":
                    res.Passed = false;
                    res.Feedback = "Файл классифицирован как текстовый файл Word. Файл представляет потенциальную опасность, " +
                        "так как может содержать опасные для устройства макросы. Для безопасной работы с файлом вы можете скачать его, " +
                        "используя опцию меню \"Безопасное скачивание\"";
                    break;
                case "excel":
                    res.Passed = false;
                    res.Feedback = "Файл классифицирован как файл Excel. Файл представляет потенциальную опасность, " +
                        "так как может содержать опасные для устройства макросы. Для безопасной работы с файлом вы можете скачать его, " +
                        "используя опцию меню \"Безопасное скачивание\"";
                    break;
                case "presentation":
                    res.Passed = false;
                    res.Feedback = "Файл классифицирован как файл презентации. Файл представляет потенциальную опасность, " +
                        "так как может содержать опасные для устройства макросы. Для безопасной работы с файлом вы можете скачать его, " +
                        "используя опцию меню \"Безопасное скачивание\"";
                    break;
                case "pdf":
                    res.Feedback = "Файл классифицирован как файл PDF. Сам файл не представляет опасности, но может содержать опасный текст и/или ссылки";
                    break;
                case "unknown":
                default:
                    res.Passed = false;
                    res.FileType = "unknown";
                    res.Feedback = "Не удалось установить тип файла. Файл классифицирован как потенциально опасный.";
                    break;
            }
            res.UpdateFields();
            return res;
        }

        public static Responce Checker_AllFiles(List<MessagePart> files)
        {
            Responce res = new Responce("Модуль проверки файлов");
            if (files.Count == 0)
            {
                res.Feedback = "В письме отсутствуют вложенные файлы";
                res.Passed = true;
                res.UpdateFields();
                return res;
            }
            res.Feedback = string.Format("Количество вложений в письме: {0}\nДетализация тестирования файлов:\n\n", files.Count);
            foreach (MessagePart file in files)
            {
                Responce file_res = Checker_File(file);
                res.Passed &= file_res.Passed;
                res.Feedback += string.Format(" - {0}: {1}\n", file.FileName, file_res.Feedback);
                if (file_res.FileType == "text" || file_res.FileType == "pdf")
                {
                    Responce link_res = Checker_FileText(file, file_res.FileType);
                    var shifted_feedback = new List<string>();
                    foreach (string s in link_res.Feedback.Split('\n'))
                    {
                        shifted_feedback.Add("   " + s);
                    }
                    res.Feedback += string.Join("\n", shifted_feedback) + "\n";
                    res.Passed &= link_res.Passed;
                }
                else if (file_res.FileType == "image")
                {
                    Responce link_res = Checker_OCR(file);
                    var shifted_feedback = new List<string>();
                    foreach (string s in link_res.Feedback.Split('\n'))
                    {
                        shifted_feedback.Add("   " + s);
                    }
                    res.Feedback += string.Join("\n", shifted_feedback) + "\n";
                    res.Passed &= link_res.Passed;
                }
            }
            res.UpdateFields();
            return res;
        }

        private static string ClassifyFile(string raw_type)
        {
            string general = raw_type.Split('/')[0];
            string spec = raw_type.Split('/')[1];

            switch (general)
            {
                case "audio":
                case "video":
                case "font":
                case "image":
                    return general;
                case "text":
                    if (spec == "javascript" || spec == "xml")
                        return "application";
                    else return "text";
                case "application":
                    if (FindSubstrings(spec, new List<string> { "rar", "tar", "7z", "freearc" }) ||
                        (spec.IndexOf("zip") > -1 && spec.IndexOf("epub") == -1)) //архивы
                        return "archive";
                    else if (FindSubstrings(spec, new List<string> { "msword", "wordprocessingml", "rtf", "text" }))
                        return "word";
                    else if (FindSubstrings(spec, new List<string> { "excel", "spreadsheet" }))
                        return "excel";
                    else if (spec.IndexOf("presentation") > -1)
                        return "presentation";
                    else if (spec == "pdf")
                        return "pdf";
                    else return "application";
                default:
                    return "unknown";
            }
        }

        private static bool FindSubstrings(string str, List<string> substr)
        {
            bool found = false;
            foreach (string sub in substr)
            {
                found |= str.IndexOf(sub) > -1;
            }
            return found;
        }

        public static Responce Checker_FileText(MessagePart file, string type)
        {
            var regex = new System.Text.RegularExpressions.Regex(@"(ht|f)tp(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&%\$#_]*)?");
            var path = FileInteraction.File_BGDownload(file);
            var links = new List<string>();
            switch (type)
            {
                case "pdf":
                    PdfDocument pdfDoc = new PdfDocument();
                    pdfDoc.LoadFromFile(path);
                    StringBuilder content = new StringBuilder();
                    foreach (PdfPageBase page in pdfDoc.Pages)
                    {
                        content.Append(page.ExtractText());
                    }
                    pdfDoc.Close();

                    string actualText = content.ToString();
                    foreach (var l in regex.Matches(actualText))
                    {
                        Uri uri = new Uri(l.ToString());
                        links.Add(uri.Host);
                    }
                    break;
                case "text":
                default:
                    using (StreamReader sr = new StreamReader(path))
                    {
                        string line;
                        while ((line = sr.ReadLine()) != null)
                        {
                            foreach (var l in regex.Matches(line))
                            {
                                Uri uri = new Uri(l.ToString());
                                links.Add(uri.Host);
                            }
                        }
                        sr.Close();
                    }
                    break;
            }
            var res = CheckAllDomains(links);
            res.ModuleName = "Модуль проверки текста файла";
            FileInteraction.File_Delete(path);
            return res;
        }

        public static Responce Checker_PageText(string text)
        {
            var regex = new System.Text.RegularExpressions.Regex(@"(ht|f)tp(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&%\$#_]*)?");
            var links = new List<string>();
            foreach (var l in regex.Matches(text))
            {
                string link = l.ToString();
                if (Uri.IsWellFormedUriString(link, UriKind.RelativeOrAbsolute))
                {
                    Uri uri = new Uri(link);
                    links.Add(uri.Host);
                }
            }
            var res = CheckAllDomains(links);
            res.ModuleName = "Модуль проверки текста тела письма";
            return res;
        }

        public static Responce CheckAllDomains(List<string> domains)
        {
            domains = new HashSet<string>(domains).ToList();
            var res = new Responce("Модуль проверки доменов ссылок");
            if (domains.Count == 0)
            {
                res.Feedback = "Модуль проверки не обнаружил ссылок";
                res.Passed = true;
                res.UpdateFields();
                return res;
            }
            res.Feedback = string.Format("Доменов, обнаруженных модулем: {0}\n", domains.Count);
            foreach (string l in domains)
            {
                try
                {
                    string link_ip = Dns.GetHostEntry(l).AddressList[0].ToString();
                    Responce dns_res = Checker_DNSBL(link_ip);
                    res.Passed &= dns_res.Passed;
                    res.Feedback += string.Format(" - Домен {0} {1}прошел проверку модулем DNSBL\n", l, dns_res.Passed ? "" : "не ");
                }
                catch
                {
                    res.Feedback += string.Format(" - Домен {0} не удалось разрешить DNS\n", l);
                }
            }
            res.UpdateFields();
            return res;
        }

        public static Responce Checker_OCR(MessagePart file)
        {
            string path = FileInteraction.File_BGDownload(file);
            var img = Pix.LoadFromFile(path);
            FileInteraction.File_Delete(path);

            var engine = new TesseractEngine(@".", "eng+rus", EngineMode.Default);
            var page = engine.Process(img);
            var text = page.GetText();

            var responce = Checker_PageText(text);
            responce.Feedback += "\nПроцент уверенности модуля OCR: " + page.GetMeanConfidence()*100 + "%\n\nОбнаруженный текст:\n" + text;
            responce.ModuleName = "Модуль OCR";

            return responce;
        }
    }
}