﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using OpenPop.Pop3;
using OpenPop.Mime;
using System.Collections.ObjectModel;
using mshtml;

namespace SafeMail
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public class Letter
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string From { get; set; }
    }

    public class CheckedFile
    {
        public Responce Responce { get; set; }
        public MessagePart File { get; set; }
    }
    public partial class MainWindow : Window
    {
        private string login; //только для отображения на главном экране
        private string email;
        private string password;

        private string hostname = "pop.gmail.com";
        private bool useSsl = true;
        private int port = 995;

        private List<Message> msgCache;
        private int msgTotal = 0;
        private const int msgToGet = 25;
        public MainWindow()
        {
            InitializeComponent();
            FetchMessages(true);
            emailLabel.Content = "Вы вошли как " + login;
        }

        public MainWindow(string login, string password)
        {
            InitializeComponent();
            this.login = login;
            this.password = password;
            email = "recent:" + login;
            FetchMessages(true);
            emailLabel.Content = "Вы вошли как " + login;
        }

        private void settingsButton_Click(object sender, RoutedEventArgs e)
        {
            tabControl.SelectedItem = settingsPanel;
            settingsButton.IsEnabled = false;
            viewButton.IsEnabled = true;
        }

        private void viewButton_Click(object sender, RoutedEventArgs e)
        {
            tabControl.SelectedItem = viewPanel;
            settingsButton.IsEnabled = true;
            viewButton.IsEnabled = false;
        }

        public void FetchMessages(bool firstLoad = false)
        {
            int added = 0;

            using (Pop3Client client = new Pop3Client())
            {
                client.Connect(hostname, port, useSsl);
                client.Authenticate(email, password, AuthenticationMethod.Auto);

                if (firstLoad)
                {
                    msgTotal = client.GetMessageCount();
                    msgCache = new List<Message>(msgTotal);
                    for (int i = msgTotal; i > Math.Max(msgTotal - msgToGet, 0); i--)
                    {
                        msgCache.Add(client.GetMessage(i));
                    }
                }
                else
                {
                    int tc = client.GetMessageCount();
                    Message msg = client.GetMessage(tc);
                    Message last_msg = msgCache[0];
                    while (tc > 0 && msg.Headers.MessageId != last_msg.Headers.MessageId)
                    {
                        msgCache = msgCache.Prepend(msg).ToList();
                        added++;
                        tc--;
                        if (tc > 0) msg = client.GetMessage(tc);
                    }
                }
            }

            var letters = new ObservableCollection<Letter>();
            foreach (Message m in msgCache)
            {
                letters.Add(new Letter { Id = m.Headers.MessageId, Title = m.Headers.Subject, From = m.Headers.From.ToString() });
            }
            int storedSelection = letterListBox.SelectedIndex;
            letterListBox.SelectedIndex = -1;
            letterListBox.ItemsSource = null;
            letterListBox.ItemsSource = letters;
            letterListBox.Items.Refresh();
            if (storedSelection != -1 && !firstLoad)
            {
                letterListBox.SelectedIndex = storedSelection + added;
            }
        }

        private void updateButton_Click(object sender, RoutedEventArgs e)
        {
            FetchMessages();
        }

        private void letterListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Message msg;
            try
            {
                msg = msgCache[(sender as ListBox).SelectedIndex];
            }
            catch
            {
                return;
            }
            topicLabelView.Content = "Тема: " + msg.Headers.Subject;
            dateLabelView.Content = "Дата и время отправки: " + msg.Headers.DateSent;
            senderLabelView.Content = "Отправитель: " + msg.Headers.From.ToString();

            string html_content;
            try { html_content = msg.FindFirstHtmlVersion().GetBodyAsText(); }
            catch (Exception ex) { html_content = "Не удалось получить HTML-содержимое: " + ex.ToString(); }
            emailTextView.NavigateToString(html_content);

            attachmentStackPanel.Children.Clear();
            var att = msg.FindAllAttachments();
            bool alter = false;
            foreach (MessagePart mp in att)
            {
                var elem = new Label
                {
                    Content = mp.FileName,
                    Background = new SolidColorBrush(Color.FromArgb(alter ? (byte)16 : (byte)64, 0, 0, 0))
                };
                var cm = new ContextMenu();
                var emp = new CheckedFile { Responce = FilterModules.Checker_File(mp), File = mp };
                cm.Items.Add(new MenuItem() { Header = "Информация о файле", Command = FileInteraction.InfoCommand, CommandParameter = emp });
                cm.Items.Add(new MenuItem() { Header = "Скачать файл", Command = FileInteraction.DownloadCommand, CommandParameter = emp });
                if (!emp.Responce.Passed)
                {
                    cm.Items.Add(new MenuItem() { Header = "Безопасное скачивание", Command = FileInteraction.SafeDownloadCommand, CommandParameter = emp });
                }
                elem.ContextMenu = cm;
                alter = !alter;
                attachmentStackPanel.Children.Add(elem);
            }
            
            string sender_ip;
            try
            {
                sender_ip = Dns.GetHostEntry(msg.Headers.From.Address.Split('@')[1]).AddressList[0].ToString();
            }
            catch 
            {
                //берем только последние две части адреса (потому что домены типа mail.domain.com могут не разрешаться DNS)
                var split_domain = msg.Headers.From.Address.Split('@')[1].Split('.');
                string domain = split_domain[split_domain.Length - 2] + '.' + split_domain[split_domain.Length - 1]; 
                sender_ip = Dns.GetHostEntry(domain).AddressList[0].ToString();
            }

            var results = new ObservableCollection<Responce>();
            if (fileCheckbox.IsChecked == true) results.Add(FilterModules.Checker_AllFiles(att));
            if (DNSBLCheckbox.IsChecked == true) results.Add(FilterModules.Checker_DNSBL(sender_ip));
            if (locationCheckbox.IsChecked == true) results.Add(FilterModules.Checker_Location(this, sender_ip));
            if (textCheckbox.IsChecked == true) results.Add(FilterModules.Checker_PageText(html_content));
            resultListBox.ItemsSource = results;
        }

        private void emailTextView_LoadCompleted(object sender, NavigationEventArgs e)
        {
            var webBrowser = sender as WebBrowser;
            if (webBrowser == null)
            {
                return;
            }
            var doc = (IHTMLDocument2)webBrowser.Document;
            if (doc.charset == "utf-8") return;

            doc.charset = "utf-8";
            webBrowser.NavigateToString(getBrowserPageData());
        }

        private string getBrowserPageData()
        {
            Message msg = msgCache[letterListBox.SelectedIndex];
            try
            {
                return msg.FindFirstHtmlVersion().GetBodyAsText();
            }
            catch
            {
                return msg.FindFirstPlainTextVersion().GetBodyAsText();
            }

        }

        private void settingChanged(object sender, RoutedEventArgs e)
        {
            reselectItem();
        }

        private void settingChanged(object sender, SelectionChangedEventArgs e)
        {
            reselectItem();
        }

        private void settingChanged(object sender, TextChangedEventArgs e)
        {
            reselectItem();
        }

        private void reselectItem()
        {
            int tmp = letterListBox.SelectedIndex;
            letterListBox.SelectedIndex = -1;
            letterListBox.SelectedIndex = tmp;
        }
    }
}