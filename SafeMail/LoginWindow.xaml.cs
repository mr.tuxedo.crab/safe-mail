﻿using OpenPop.Pop3;
using OpenPop.Pop3.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SafeMail
{
    /// <summary>
    /// Логика взаимодействия для LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void loginButton_Click(object sender, RoutedEventArgs e)
        {
            IsEnabled = false;
            using (Pop3Client client = new Pop3Client())
            {
                client.Connect("pop.gmail.com", 995, true);
                try
                {
                    client.Authenticate(loginTextBox.Text, passwordTextBox.Password, AuthenticationMethod.Auto);
                    var main = new MainWindow(loginTextBox.Text, passwordTextBox.Password);
                    Close();
                    main.Show();
                }
                catch (InvalidLoginException)
                {
                    IsEnabled = true;
                    errorLabel.Content = "Неверный логин или пароль";
                }
                catch (LoginDelayException)
                {
                    IsEnabled = true;
                    errorLabel.Content = "Слишком много попыток входа";
                }
                catch (PopServerLockedException)
                {
                    IsEnabled = true;
                    errorLabel.Content = "Почтовый ящик заблокирован";
                }
                catch (Exception)
                {
                    IsEnabled = true;
                    errorLabel.Content = "Почтовый сервер недоступен";
                }
            }
        }
    }
}
