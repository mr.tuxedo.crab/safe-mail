﻿using OpenPop.Mime;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Microsoft.Office.Interop.Excel;
using System.IO;
using Ookii.Dialogs.Wpf;

namespace SafeMail
{
    class FileInteraction
    {
        public static ICommand InfoCommand
        {
            get { return new DelegateCommand<object>(InfoFunc, _ => true); }
        }

        private static void InfoFunc(object context)
        {
            var emp = (CheckedFile)context;
            var file = emp.File;
            var res = emp.Responce;
            MessageBox.Show(string.Format("Тип контента (из заголовка): \"{0}\"\nТип файла (классификатор): \"{1}\"\n" +
                "Название файла: \"{2}\"\nРешение модуля фильтрации файлов: {3}",
                file.ContentType, res.FileType, file.FileName, res.Feedback), "Data", MessageBoxButton.OK);
        }
        public static ICommand DownloadCommand
        {
            get { return new DelegateCommand<object>(DownloadFunc, _ => true); }
        }

        private static void DownloadFunc(object context)
        {
            var emp = (CheckedFile)context;
            if (!emp.Responce.Passed)
            {
                var res = MessageBox.Show("Вы уверены, что хотите скачать этот файл?\nФайл был классифицирован как потенциально опасный\n" +
                    "Если для данного файла есть функция безопасного скачивания, то предпочтительнее скачать файл с использованием этой функции\n" +
                    "Нажмите \"Да\", только если уверены, что файл безопасен", 
                    "Вы уверены?", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (res == MessageBoxResult.No) return;
            }
            File_Download(emp.File);
        }

        public static ICommand SafeDownloadCommand
        {
            get { return new DelegateCommand<object>(SafeDownloadFunc, _ => true); }
        }

        private static void SafeDownloadFunc(object context)
        {
            var emp = (CheckedFile)context;
            var file = emp.File;
            var res = emp.Responce;
            switch (res.FileType)
            {
                case "excel":
                    SafeDownload_Excel(file);
                    break;
                case "word":
                    SafeDownload_Word(file);
                    break;
                case "presentation":
                    SafeDownload_Presentation(file);
                    break;
                default:
                    break;
            }
        }

        private static void SafeDownload_Word(MessagePart file)
        {
            var path = File_BGDownload(file);
            var wapp = new Microsoft.Office.Interop.Word.Application
            {
                AutomationSecurity = Microsoft.Office.Core.MsoAutomationSecurity.msoAutomationSecurityForceDisable,
                ActivePrinter = "Microsoft Print to PDF"
            };
            var wd = wapp.Documents.Open(path);
            var res_name = file.FileName.Substring(0, file.FileName.LastIndexOf('.')) + ".pdf";
            var sp = AppDomain.CurrentDomain.BaseDirectory + "tmp\\" + res_name;
            wd.PrintOut(PrintToFile: true, OutputFileName: sp);

            wd.Close(SaveChanges: false);
            wapp.Quit();

            File_Delete(path);
        }

        private static void SafeDownload_Excel(MessagePart file)
        {
            var path = File_BGDownload(file);
            var xapp = new Microsoft.Office.Interop.Excel.Application
            {
                AutomationSecurity = Microsoft.Office.Core.MsoAutomationSecurity.msoAutomationSecurityForceDisable
            };
            var wb = xapp.Workbooks.Open(path);
            foreach (Worksheet ws in wb.Worksheets)
            {
                PageSetup ps = ws.PageSetup;
                ws.PageSetup.Orientation = XlPageOrientation.xlLandscape;
                ws.PageSetup.Order = XlOrder.xlDownThenOver;
                ws.PageSetup.FitToPagesWide = 1;
                ws.PageSetup.FitToPagesTall = 50;
                ws.PageSetup.Zoom = false;
            }
            var res_name = file.FileName.Substring(0, file.FileName.LastIndexOf('.')) + ".pdf";
            var sp = AppDomain.CurrentDomain.BaseDirectory + "tmp\\" + res_name;
            wb.PrintOutEx(PrintToFile:true, PrToFileName:sp, ActivePrinter:"Microsoft Print to PDF");

            wb.Close(SaveChanges:false);
            xapp.Quit();

            File_Delete(path);
        }

        private static void SafeDownload_Presentation(MessagePart file)
        {
            var path = File_BGDownload(file);
            var papp = new Microsoft.Office.Interop.PowerPoint.Application
            {
                AutomationSecurity = Microsoft.Office.Core.MsoAutomationSecurity.msoAutomationSecurityForceDisable
            };
            var pp = papp.Presentations.Open(path, WithWindow:Microsoft.Office.Core.MsoTriState.msoFalse);
            var res_name = file.FileName.Substring(0, file.FileName.LastIndexOf('.')) + ".pdf";
            var sp = AppDomain.CurrentDomain.BaseDirectory + "tmp\\" + res_name;
            pp.PrintOut(PrintToFile: sp);

            pp.Close();
            papp.Quit();

            File_Delete(path);
        }

        public static string File_BGDownload(MessagePart file)
        {
            var ap = AppDomain.CurrentDomain.BaseDirectory + "tmp";
            Directory.CreateDirectory(ap);
            var file_name = Guid.NewGuid() + "." + file.FileName.Split('.').Last();
            var file_path = Path.Combine(ap, file_name);
            file.Save(new FileInfo(file_path));

            return file_path;
        }

        public static void File_Delete(string path) //cursed
        {
            bool deleted = false;
            while (!deleted)
            {
                try
                {
                    using (Stream stream = new FileStream(path, FileMode.Open))
                    {
                        stream.Dispose();
                    }
                    File.Delete(path);
                    deleted = true;
                }
                catch
                {
                    deleted = false;
                }
            }
        }

        private static string File_GetSavePath()
        {
            var dialog = new VistaFolderBrowserDialog();
            string path = "";
            if (dialog.ShowDialog().GetValueOrDefault())
            {
                path = dialog.SelectedPath;
            }
            return path;
        }

        private static void File_Download(MessagePart file)
        {
            string path = Path.Combine(File_GetSavePath(), file.FileName);
            file.Save(new FileInfo(path));
            MessageBox.Show(string.Format("Файл был успешно загружен\nПуть: \"{0}\"", path), "Файл загружен", MessageBoxButton.OK);
        }
    }
}